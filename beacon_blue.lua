modWPBeacons.register_beacon("waypoint_beacons:blue",{
	description = modWPBeacons.translate("Blue Beacon"),
	protected = true,
	--light_height = 50,

	tiles_beacon = {"bluebeacon.png"},
	tiles_base = {"bluebase.png"},
	tiles_beam = {"bluebeam.png"},

	node_base = "waypoint_beacons:bluebase",
	node_beam = "waypoint_beacons:bluebeam",
	
	particle = "blueparticle.png",
	dye_collor = "dye:blue",
	
	damage_per_second = 4 * 2,
	post_effect_color = {a=192, r=64, g=64, b=255},
	
	alias = {modWPBeacons.translate("beaconblue"), "sinalizadorazul"},
})
