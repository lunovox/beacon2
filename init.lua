modWPBeacons = {
	modname = minetest.get_current_modname(),
	modpath = minetest.get_modpath(
		minetest.get_current_modname()
	),
}

dofile(modWPBeacons.modpath.."/api.lua")

dofile(modWPBeacons.modpath.."/beacon_blue.lua")
dofile(modWPBeacons.modpath.."/beacon_green.lua")
dofile(modWPBeacons.modpath.."/beacon_purple.lua")
dofile(modWPBeacons.modpath.."/beacon_red.lua")

minetest.log('action',"["..minetest.get_current_modname():upper().."] Loaded!")
