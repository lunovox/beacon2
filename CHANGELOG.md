# **CHANGELOG:**

## WAYPOINT-BEACONS v3.0.0 : forked Lunovox Version
* Added waypoint with editable name. (from Lunovox)
* Refactored to ContentDB.

## Beacons v2.0
* Added Api for Beacons
* Added light height of beacon in init
* Added protection(optional) of beacon in init (owner, or admin, or ownerland per mod landrush: can move this beacon if protected = true)
* Added light only in air
* Added damage of light in player
* Added file description.txt
* Added main screenshot

## Beacons v1.1
* Cleaned up the messy beamgen code with for loops
* Renamed Purple --> Violet Beacon to reduce confusion with dye
* Added README.txt, For some reason I forgot to include it in the first release
* Changed "Screenshots" --> "Crafts" Folder, now only contains craft recipes
* Added little message in log/console when loaded "[OK] Beacons"

## Beacons v1.0
* Added Red Beacon
* Added Blue Beacon
* Added Purple Beacon
* Added Green Beacon