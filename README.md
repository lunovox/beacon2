![](screenshot.png)
# [WAYPOINT BEACONS]

[![minetest_icon]][minetest_link] This mod adds beacons with renamable waypoints to the server that can be used as personal landmarks.

## **Dependencies:**
| Mod Name | Dependency Type | Descryption |
| :--: | :--: | :-- |
| default | Mandatory |  [Minetest Game] Included. | 
| dye | Mandatory | [Minetest Game] Included. |
| [intllib][mod_intllib] | Optional | Facilitates the translation of this mod into your native language, or other languages. |
| [tradelands][mod_tradelands] | Optional | Protection of land on payment of periodic rate. The Minetest player can set permissions of PVP for his terrain. Need to craft a land protection charter. | 
| [eurn][mod_eurn] | Optional | Adds a presidential election system and an Electronic Urn as an item that helps collect votes. | 
| [landrush] | Optional | Other Protection of Land | 

## **Developers:**
 * AgentNagel42
 * [Sam Hocevar](sam@hocevar.net) 
 * Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](https://qoto.org/@lunovox), [WebChat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [audio conference](mumble:mumble.disroot.org), [more contacts](https://libreplanet.org/wiki/User:Lunovox)

## **License:**
* [![license_code_icon]][license_code_link]
* [![license_media_icon]][license_media_link]
 
## **Official Video:** 
 * See in [youtube].

## **Forum Topic:** 
 * https://forum.minetest.net/viewtopic.php?f=11&t=12041
 
## **Changelog:** 
See in [CHANGELOG.md].



[CHANGELOG.md]:./CHANGELOG.md
[WAYPOINT BEACONS]:https://gitlab.com/lunovox/waypoint-beacons
[license_code_icon]:https://img.shields.io/static/v1?label=LICENSE%20CODE&message=GNU%20AGPL%20v3.0&color=yellow
[license_code_link]:https://gitlab.com/lunovox/waypoint-beacons/-/raw/master/LICENSE_CODE
[license_media_icon]:https://img.shields.io/static/v1?label=LICENSE%20MEDIA&message=CC%20BY-SA-4.0&color=yellow
[license_media_link]:https://gitlab.com/lunovox/waypoint-beacons/-/raw/master/LICENSE_MEDIA
[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Mod&color=brightgreen
[minetest_link]:https://minetest.net
[Minetest Game]:https://content.luanti.org/packages/Minetest/minetest_game/?protocol_version=45
[mod_eurn]:https://gitlab.com/lunovox/e-urn
[mod_intllib]:https://content.luanti.org/packages/kaeza/intllib/
[mod_tradelands]:https://gitlab.com/Lunovox/tradelands
[youtube]:https://youtu.be/NA1T4Vu6f5g