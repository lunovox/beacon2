modWPBeacons.register_beacon("waypoint_beacons:green",{
	description = modWPBeacons.translate("Green Beacon"),
	protected = true,
	--light_height = 50,

	tiles_beacon = {"greenbeacon.png"},
	tiles_base = {"greenbase.png"},
	tiles_beam = {"greenbeam.png"},

	node_base = "waypoint_beacons:greenbase",
	node_beam = "waypoint_beacons:greenbeam",
	
	particle = "greenparticle.png",
	dye_collor = "dye:green",
	
	damage_per_second = 4 * 2,
	post_effect_color = {a=192, r=64, g=255, b=64},

	alias = {modWPBeacons.translate("beacongreen"), "sinalizadorverde"},
})
