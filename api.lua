modWPBeacons.beacon_atual = {}

if minetest.get_modpath("intllib") then
	modWPBeacons.translate = intllib.Getter()
else
	modWPBeacons.translate = function(txt) return (txt or "") end
end

modWPBeacons.isParticleShowed = function()
	if type(modWPBeacons.show_particles) ~= "boolean" then
		modWPBeacons.show_particles = (minetest.settings:get("waypoint_beacons.show_particles") ~= "false")
		minetest.settings:set_bool("waypoint_beacons.show_particles", modWPBeacons.show_particles)
		--minetest.settings:write() --there is no need to automatically write the settings to the 'minetest.conf' file.
	end
	return modWPBeacons.show_particles
end

modWPBeacons.getMaxLaserHeight = function()
	if type(modWPBeacons.max_laser_height) ~= "number" then
		modWPBeacons.max_laser_height = tonumber(minetest.settings:get("waypoint_beacons.max_laser_height"))
		if type(modWPBeacons.max_laser_height) ~= "number"
		   or modWPBeacons.max_laser_height < 0
		   or modWPBeacons.max_laser_height > 100
		then
		   modWPBeacons.max_laser_height = 50
		end
		minetest.settings:set("waypoint_beacons.max_laser_height", tostring(modWPBeacons.max_laser_height))
		--minetest.settings:write() --there is no need to automatically write the settings to the 'minetest.conf' file.
	end
	return modWPBeacons.max_laser_height
end

minetest.register_node("waypoint_beacons:empty", {
	description = modWPBeacons.translate("Unactivated Beacon"),
	tiles = {"emptybeacon.png"},
	light_source = 3,
	groups = {cracky=3,oddly_breakable_by_hand=3},
	drop = "waypoint_beacons:empty",
})

minetest.register_craft({
	output = 'waypoint_beacons:empty',
	recipe = {
		{'default:steel_ingot'				, 'default:glass'		, 'default:steel_ingot'},
		{'default:mese_crystal_fragment'	, 'default:torch'		, 'default:mese_crystal_fragment'},
		{'default:obsidian'					, 'default:obsidian'	, 'default:obsidian'},
	}
})

modWPBeacons.doStorage = function()
   modWPBeacons.modStorage = minetest.get_mod_storage()
end

modWPBeacons.getPlayerBeacons = function(playername)
	--minetest.log('error',dump(playername))	
	if type(core.get_mod_storage) == "function" then
		if modWPBeacons.database == nil then
			local txtBeacons = modWPBeacons.modStorage:get_string("waypoint_beacons")
			modWPBeacons.database = minetest.deserialize(txtBeacons)
		end
	end
	if modWPBeacons.database == nil then
		modWPBeacons.database = {}
	end
	if type(playername)=="string" and playername~="" then
		if modWPBeacons.database[playername] ~= nil then 
			return modWPBeacons.database[playername]
		end
	end
end

modWPBeacons.setPlayerBeacons = function(playername, tblPlayerBeacons)
	if type(playername)=="string" and playername~="" then
		if type(tblPlayerBeacons)=="table" then
			if modWPBeacons.database == nil then
				modWPBeacons.database = {}
			end
			
			modWPBeacons.database[playername] = tblPlayerBeacons
			
			if type(core.get_mod_storage) == "function" then
				
				modWPBeacons.modStorage:set_string(
					"waypoint_beacons", 
					minetest.serialize(modWPBeacons.database)
				)
			end
		end
	end
end

modWPBeacons.can_interact = function(pos, playername)
	--minetest.log('error',dump(playername))	
	if type(playername)=="string" and playername~="" then
		local meta = minetest.get_meta(pos);
		local ownername = meta:get_string("owner") 
		if ownername=="" or playername == ownername 
			or minetest.get_player_privs(playername).protection_bypass
			or (minetest.global_exists("landrush") and landrush.get_owner(pos)~=nil and landrush.can_interact(pos, playername))
			or (minetest.global_exists("modTradeLands") and modTradeLands.canInteract(pos, playername))
			or (minetest.global_exists("modEUrn") and modEUrn.getPresidentName()==playername)
		then
			return true
		end
	end
	return false
end

modWPBeacons.getFormspec = function(playername, nameBeaconPos)
	local tblPlayerBeacons = modWPBeacons.getPlayerBeacons(playername)
	if type(tblPlayerBeacons)=="table" then
		if type(tblPlayerBeacons[nameBeaconPos])=="table"  then
			if type(tblPlayerBeacons[nameBeaconPos].name)=="string" then
				return "size[8,1.5]"..
				--"bgcolor[#FFFFFFBB;false]"..
				default.gui_bg..
				default.gui_bg_img..
				default.gui_slots..
				--"label[0,0;Nome do Farol]"..
				"field[0.5,0.5;7.5,1;txtName;"..modWPBeacons.translate("Beacon Name")..";"..minetest.formspec_escape(tblPlayerBeacons[nameBeaconPos].name or "").."]"..
				"button_exit[3.0,1.0;2,1;btnRename;"..modWPBeacons.translate("RENAME").."]"..
				default.get_hotbar_bg(0, 4.25)
			else
				minetest.chat_send_all("type(charBeacons[nameBeaconPos].name)="..type(charBeacons[nameBeaconPos].name))
			end
		else
			minetest.chat_send_all("type(charBeacons[nameBeaconPos])="..type(charBeacons[nameBeaconPos]))
		end
	end
end

modWPBeacons.register_beacon = function(nodename, def)
	minetest.register_node(nodename, {
		description = def.description,
		tiles = def.tiles_beacon,
		light_source = 13,
		groups = {cracky=3,oddly_breakable_by_hand=3},
		drop = nodename,
		after_place_node = function(pos, placer, itemstack, pointed_thing)
			local playername = placer:get_player_name()
			local meta = minetest.get_meta(pos);
			meta:set_string("owner", playername)
			meta:set_string("infotext",  def.description.." of '"..playername.."'")		
			--if minetest.get_modpath("lib_savevars") then
			if type(core.get_mod_storage) == "function" then
				local posAbove = pointed_thing.above --acima
				local posUnder = pointed_thing.under --abaixo
				--local nameBeaconPos = "x"..posUnder.x..",y"..posUnder.y..",z"..posUnder.z
				local nameBeaconPos = "BEACON ("..posAbove.x..","..posAbove.y..","..posAbove.z..")"
				meta:set_string("beacon_namepos",  nameBeaconPos)
				local charBeacons = modWPBeacons.getPlayerBeacons(playername)
				
				if type(charBeacons)~="table" then charBeacons={} end
				
				charBeacons[nameBeaconPos]={}
				charBeacons[nameBeaconPos].pos = posUnder
				charBeacons[nameBeaconPos].color = def.post_effect_color
				charBeacons[nameBeaconPos].name = nameBeaconPos

				--modWPBeacons.setPlayerBeacons(playername, charBeacons)
				modWPBeacons.setPlayerBeacons(playername, charBeacons)
				modWPBeacons.huds.add_waypoints(placer)
			end
		end,
		on_destruct = function(pos) --remove the beam above a source when source is removed
			for i=1,(def.light_height or modWPBeacons.getMaxLaserHeight()) do
				local node = minetest.get_node({x=pos.x, y=pos.y+i, z=pos.z})
				if node and node.name==def.node_base or node.name==def.node_beam then
					minetest.remove_node({x=pos.x, y=pos.y+i, z=pos.z}) --thanks Morn76 for this bit of code!
				end
			end
			--if minetest.get_modpath("lib_savevars") then
			if type(core.get_mod_storage) == "function" then
				local meta = minetest.get_meta(pos);
				local ownername = meta:get_string("owner")
				local ownerplayer = minetest.get_player_by_name(ownername)
				local nameBeaconPos = meta:get_string("beacon_namepos")
				local charBeacons = modWPBeacons.getPlayerBeacons(ownername)
				if type(charBeacons)=="table" and type(charBeacons[nameBeaconPos])=="table" then
					charBeacons[nameBeaconPos] = nil
					if ownerplayer~= nil and ownerplayer:is_player() then
						ownerplayer:hud_remove(modWPBeacons.huds.waypoints[ownername][nameBeaconPos])
					end
				end
				modWPBeacons.setPlayerBeacons(ownername, charBeacons)
			end
		end,
		can_dig = function(pos, player)
			if def.protected or false then
				return modWPBeacons.can_interact(pos, player:get_player_name())
			end
			return true
		end,
		on_rightclick = function(pos, node, clicker)
			--if type(modsavevars)~="nil" then
			if type(core.get_mod_storage) == "function" then
				local meta = minetest.get_meta(pos)
				local playername = clicker:get_player_name()
				local ownername = meta:get_string("owner")
				local nameBeaconPos = meta:get_string("beacon_namepos")
			
				if playername == ownername then 
					modWPBeacons.beacon_atual[playername] = nameBeaconPos
					minetest.show_formspec(
						playername,
						"beacon_"..playername,
						modWPBeacons.getFormspec(playername, nameBeaconPos)
					)
				end
			end
		end,
	})
	
	minetest.register_craft({
		output = nodename,
		recipe = {
			{''					, def.dye_collor	, ''},
			{def.dye_collor	, "waypoint_beacons:empty"	, def.dye_collor},
			{''					, def.dye_collor	, ''},
		}
	})
	
	minetest.register_craft({
		output = "waypoint_beacons:empty",
		recipe = {
			{''				, "dye:white"	, ''},
			{"dye:white"	, nodename		, "dye:white"},
			{''				, "dye:white"	, ''},
		}
	})
	
	if def.alias and #def.alias >=1 then
		for i=1,#def.alias do
			minetest.register_alias(def.alias[i] ,nodename)
		end
	end
	
	--Blue Beam
	minetest.register_node(def.node_base, {
		visual_scale = 1.0,
		drawtype = "plantlike",
		tiles = def.tiles_base,
		paramtype = "light",
		walkable = false,
		diggable = false,
		pointable = false,
		buildable_to = true,	
		light_source = 50, --padrao = 13
		damage_per_second = def.damage_per_second or 0,
		post_effect_color = def.post_effect_color,
		groups = {not_in_creative_inventory=1},
	})

	minetest.register_node(def.node_beam, {
		visual_scale = 1.0,
		drawtype = "plantlike",
		tiles = def.tiles_beam,
		paramtype = "light",
		walkable = false,
		diggable = false,
		pointable = false,
		buildable_to = true,
		light_source = 50,
		damage_per_second = def.damage_per_second or 0,
		post_effect_color = def.post_effect_color,
		groups = {not_in_creative_inventory=1},
	})

	minetest.register_abm({
		nodenames = {nodename, def.node_base, def.node_beam},
		interval = 5,
		chance = 1,
		action = function(pos)
			if minetest.get_node(pos).name==nodename then
				pos.y = pos.y + 1 --Sobe um bloco
				if minetest.get_node(pos).name=="ignore" or minetest.get_node(pos).name=="air" then
					minetest.add_node(pos, {name=def.node_base})
					for i=1,(def.light_height or modWPBeacons.getMaxLaserHeight()) do
						if minetest.get_node({x=pos.x, y=pos.y+i, z=pos.z}).name=="ignore" 
						   or minetest.get_node({x=pos.x, y=pos.y+i, z=pos.z}).name=="air" 
						then
							minetest.add_node({x=pos.x, y=pos.y+i, z=pos.z}, {name=def.node_beam})
						else
							break
						end
					end
				end
			elseif minetest.get_node(pos).name==def.node_base 
			   or minetest.get_node(pos).name==def.node_beam 
			then
				pos.y = pos.y - 1 --Desce um bloco
				for i=0,(def.light_height or modWPBeacons.getMaxLaserHeight()) do
					if minetest.get_node({x=pos.x, y=pos.y+i, z=pos.z}).name~=nodename 
						and minetest.get_node({x=pos.x, y=pos.y+i, z=pos.z}).name~=def.node_beam 
						and minetest.get_node({x=pos.x, y=pos.y+i, z=pos.z}).name~=def.node_base 
					then
						minetest.remove_node({x=pos.x, y=pos.y+i+1, z=pos.z}) --thanks Morn76 for this bit of code!
					else
						break
					end
				end
			end
		end,
	})
	
	if modWPBeacons.isParticleShowed()
	   and type(def.particle)=="string" 
	   and def.particle~="" 
	then
		minetest.register_abm({
			nodenames = {
			   def.node_base
			   --, def.node_beam --If this line is activated it may cause lag on the server.
			}, --makes small particles emanate from the beginning of a beam
			interval = 1,
			chance = 2,
			action = function(pos, node)
				minetest.add_particlespawner({
					amount = 32,
					time = 4,
					minpos = {x=pos.x-0.25, y=pos.y-0.25, z=pos.z-0.25},
					maxpos = {x=pos.x+0.25, y=pos.y+0.25, z=pos.z+0.25},
					minvel = {x=-0.8,	y=-0.8,	z=-0.8},
					maxvel = {x=0.8,	y=0.8,	z=0.8},
					minacc = {x=0, y=0, z=0},
					maxacc = {x=0, y=0, z=0},
					minexptime = 0.5,
					maxexptime = 1,
					minsize = 1,
					maxsize = 2,
					collisiondetection = false,
					vertical = true,
					texture = def.particle,
					--playername = "singleplayer",
				})
			end,
		})
	end
end

minetest.register_on_player_receive_fields(function(sender, formname, fields)
	local playername = sender:get_player_name()
	if formname == "beacon_"..playername then
		--minetest.chat_send_all("fields="..dump(fields))
		local nameBeaconPos = modWPBeacons.beacon_atual[playername]
		if type(fields.txtName)=="string" then
			local charBeacons = modWPBeacons.getPlayerBeacons(playername)
			if type(charBeacons)=="table" then
				if type(charBeacons[nameBeaconPos])=="table"  then
					if fields.name~="" then
						charBeacons[nameBeaconPos].name = fields.txtName
					else
						charBeacons[nameBeaconPos].name = nameBeaconPos
					end
					modWPBeacons.setPlayerBeacons(playername, charBeacons)
					modWPBeacons.huds.update_waypoints(sender)
				end
			end
		end
	end
end)
--modWPBeacons.beacon_atual[playername]

--if minetest.get_modpath("lib_savevars") then
if type(core.get_mod_storage) == "function" then
	modWPBeacons.huds = {}
	modWPBeacons.huds.waypoints = {}
	modWPBeacons.huds.doHex2rgb = function(hex)
		 hex = hex:gsub("#","")
		 return tonumber("0x"..hex:sub(1,2)), tonumber("0x"..hex:sub(3,4)), tonumber("0x"..hex:sub(5,6))
	end
	modWPBeacons.huds.doRgb2hex = function(rgb)
		local colors = {rgb.r, rgb.g, rgb.b}
		local hexadecimal = '0X'

		for key, value in pairs(colors) do
			local hex = ''

			while(value > 0)do
				local index = math.fmod(value, 16) + 1
				value = math.floor(value / 16)
				hex = string.sub('0123456789ABCDEF', index, index) .. hex			
			end

			if(string.len(hex) == 0)then
				hex = '00'

			elseif(string.len(hex) == 1)then
				hex = '0' .. hex
			end

			hexadecimal = hexadecimal .. hex
		end

		return hexadecimal
	end
	modWPBeacons.huds.add_waypoints = function(player)
		if player~= nil and player:is_player() then
			local playername = player:get_player_name()
			local charBeacons = modWPBeacons.getPlayerBeacons(playername)
			--minetest.chat_send_all("type(charBeacons)="..type(charBeacons))
			if type(charBeacons)=="table" then
				if type(modWPBeacons.huds.waypoints[playername])=="nil" then modWPBeacons.huds.waypoints[playername] = {} end
				for nameBeaconPos in pairs(charBeacons) do
					local thisBeacon = charBeacons[nameBeaconPos]
					 --minetest.chat_send_all("nameBeaconPos="..dump(nameBeaconPos))
					 --minetest.chat_send_all("thisBeacon="..dump(thisBeacon))
					 
					 modWPBeacons.huds.waypoints[playername][nameBeaconPos] = player:hud_add({
						hud_elem_type = "waypoint",
						name = thisBeacon.name,
						number = modWPBeacons.huds.doRgb2hex(thisBeacon.color), 
						world_pos = thisBeacon.pos,
						text = "m", 
						precision = 1,
					})
					--[[
						{
							name = "death_"..os.date("%Y%m%d%H%M%S"),
							color = "#AF0000",
							pos = vector.round(localplayer:get_pos())
						}
					--]]
				end
			end
		end
	end
	modWPBeacons.huds.update_waypoints = function(player)
		if player~= nil and player:is_player() then
			local playername = player:get_player_name()
			local charBeacons = modWPBeacons.getPlayerBeacons(playername)
			if type(charBeacons)=="table" then
				for nameBeaconPos in pairs(charBeacons) do
					local thisBeacon = charBeacons[nameBeaconPos]
					--minetest.chat_send_all("type(modWPBeacons.huds.waypoints[playername][nameBeaconPos])="..dump(type(modWPBeacons.huds.waypoints[playername][nameBeaconPos])))
					--minetest.chat_send_all("modWPBeacons.huds.waypoints[playername][nameBeaconPos]="..dump(modWPBeacons.huds.waypoints[playername][nameBeaconPos]))
					if type(modWPBeacons.huds.waypoints[playername][nameBeaconPos])=="number" then
						player:hud_change(
							modWPBeacons.huds.waypoints[playername][nameBeaconPos], 
							"name", 
							thisBeacon.name
						)
					else
						--modWPBeacons.huds.add_waypoints(player)
					end
				end
			end
		end
	end
	minetest.register_on_joinplayer(function(player)
		modWPBeacons.huds.add_waypoints(player)
	end)
	minetest.after(3, function()
		minetest.register_globalstep(function(dtime)
			local players = minetest.get_connected_players()
			if #players >= 1 then
				local uptime = 10.0 + (#players * dtime * 2) --Não sei se 0.3 e rapido d+ ou lento d+
				if modWPBeacons.huds.time==nil or type(modWPBeacons.huds.time)~="number" then 
					modWPBeacons.huds.time = 0 
				end
				modWPBeacons.huds.time = modWPBeacons.huds.time + dtime; -- 'dtime' e sempre uma fracao entre 0 e 1
				--print("modWPBeacons.huds.time("..modWPBeacons.huds.time..") >= uptime("..uptime..")")
				if modWPBeacons.huds.time >= uptime then --atualiza a barra do jogador a cada 'uptime' segundos (se hp tiver mudado)
					--OBS.: Não sei se o valor 0.1 acima
					for _, player in ipairs(players) do
						if player~= nil and player:is_player() then
							if type(modWPBeacons.huds.update_waypoints) ~= "nil" then
								modWPBeacons.huds.update_waypoints(player)
							end
						end
					end 
					modWPBeacons.huds.time = 0
				end
			end
		end)
	end)
end

modWPBeacons.doStorage()