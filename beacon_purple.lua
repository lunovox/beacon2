modWPBeacons.register_beacon("waypoint_beacons:purple",{
	description = modWPBeacons.translate("Purple Beacon"),
	protected = true,
	--light_height = 50,

	tiles_beacon = {"purplebeacon.png"},
	tiles_base = {"purplebase.png"},
	tiles_beam = {"purplebeam.png"},

	node_base = "waypoint_beacons:purplebase",
	node_beam = "waypoint_beacons:purplebeam",
	
	particle = "purpleparticle.png",
	dye_collor = "dye:violet",
	
	damage_per_second = 4 * 2,
	post_effect_color = {a=192, r=255, g=64, b=255},

	alias = {modWPBeacons.translate("beaconpurple"), "sinalizadorpurple"},
})
